package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PasteBinCreatedPage extends AbstractPage {

    public PasteBinCreatedPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='info-top']/h1")
    private WebElement pasteTitle;

    @FindBy(xpath = "//a[@href='/archive/bash']")
    private WebElement syntaxHighlightedAsBash;

    @FindBy(xpath = "//*[@class='textarea']")
    private WebElement codeText;

    public String getPasteTitle() {
        System.out.println("Paste Name is " + pasteTitle.getText());
        return pasteTitle.getText();
    }

    public String getSyntaxHighlightedAsBash() {
        System.out.println("Syntax is highlighted as " + syntaxHighlightedAsBash.getText());
        return syntaxHighlightedAsBash.getText();
    }

    public String getCodeText() {
        System.out.println("Added text is:\n" + codeText.getText());
        return codeText.getText();
    }

    @Override
    protected AbstractPage openPage() {
        throw new RuntimeException("Please add required values to create a new paste. You cannot create an empty paste.");
    }
}
