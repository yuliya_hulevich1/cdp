package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PasteBinHomePage extends AbstractPage {

    private static final String HOMEPAGE_URL = "https://pastebin.com";

    @FindBy(xpath = "//*[@class='textarea -form js-paste-code']")
    private WebElement newPasteInput;

    @FindBy(xpath = "//*[@title='None']/parent::*")
    private WebElement syntaxHighlighting;

    @FindBy(xpath = "//*[@class='select2-results__options select2-results__options--nested']/descendant::*[contains(text(), 'Bash')]")
    private WebElement selectSyntaxHighlighting;

    @FindBy(xpath = "//*[@title='Never']/parent::*")
    private WebElement expiration;

    @FindBy(xpath = "//*[@class='select2-results__options']/descendant::*[contains(text(), '10 Minutes')]")
    private WebElement selectExpiration;

    @FindBy(xpath = "//input[@name='PostForm[name]']")
    private WebElement pasteTitle;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement createNewPasteButton;

    public PasteBinHomePage(WebDriver driver) {
        super(driver);
    }

    public PasteBinHomePage openPage() {
        driver.get(HOMEPAGE_URL);
        new WebDriverWait(driver, WAIT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='content__title -no-border']")));
        return this;
    }

    public PasteBinCreatedPage createNewPaste(String CODE, String TITLE) {
        newPasteInput.sendKeys(CODE);
        syntaxHighlighting.click();
        selectSyntaxHighlighting.click();
        expiration.click();
        selectExpiration.click();
        pasteTitle.sendKeys(TITLE);
        createNewPasteButton.click();
        new WebDriverWait(driver, WAIT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='notice -success -post-view']")));
        return new PasteBinCreatedPage(driver);
    }
}
