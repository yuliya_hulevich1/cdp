package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import page.PasteBinCreatedPage;
import page.PasteBinHomePage;

public class PasteBinTest {

    private WebDriver driver;
    private static final String CODE = "git config --global user.name  \"New Sheriff in Town\"\n" +
            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "git push origin master --force";
    private static final String TITLE = "how to gain dominance among developers";
    SoftAssert softAssert = new SoftAssert();

    @BeforeMethod(alwaysRun = true)
    public void browserSetup() {
        driver = new ChromeDriver();
    }

    @Test(description = "Bring it on task")
    public void verifyPasteTitle() {

        PasteBinHomePage page = new PasteBinHomePage(driver)
                .openPage();
        PasteBinCreatedPage createdPage = page
                .createNewPaste(CODE, TITLE);

        String actualPasteTitle = createdPage.getPasteTitle();
        String expectedPasteTitle = "how to gain dominance among developers";
        softAssert.assertEquals(actualPasteTitle, expectedPasteTitle, "Verify paste title");

        String actualPasteHighlighting = createdPage.getSyntaxHighlightedAsBash();
        String expectedPasteHighlighting = "Bash";
        softAssert.assertEquals(actualPasteHighlighting, expectedPasteHighlighting,
                "Verify paste highlighting");

        String actualPasteCodeText = createdPage.getCodeText();
        String expectedPasteCodeText = "git config --global user.name  \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        softAssert.assertEquals(actualPasteCodeText, expectedPasteCodeText,
                "Verify paste code text");
        softAssert.assertAll();

    }

    @AfterMethod(alwaysRun = true)
    public void browserTearDown() {
        driver.quit();
        driver = null;
    }
}

